// DEFINE UI Elments Variables

const form = document.querySelector('#task-form');
const taskList = document.querySelector('.collection');
const clearBtn = document.querySelector('.clear-tasks');
const filter = document.querySelector('#filter');
const taskInput = document.querySelector('#task');

loadEventListeners();

//Load All Events
function loadEventListeners(){
    //Window Load Event
    document.addEventListener('DOMContentLoaded', getTasks);
    //Form submit Event
    form.addEventListener('submit', addTask);
    //Remove Task Evnet
    taskList.addEventListener('click', removeTask);
    //Clear Task Event
    clearBtn.addEventListener('click', clearTasks);
    //Event to manage filteration of tasks
    filter.addEventListener('keyup', filterTasks);
}
function getTasks(){
    let tasks;
    if(localStorage.getItem('tasks') === null){
        tasks = [];
    }else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    console.log(tasks);
    tasks.forEach(function(task){
        const li = document.createElement('li');
        li.className = "collection-item";
        li.appendChild(document.createTextNode(task));
        
        const link = document.createElement('a');
        link.className = 'delete-item secondary-content';
        link.setAttribute('href', '#');
        link.innerHTML = "<i class='fa fa-trash'></i>";
        
        li.appendChild(link);
        
        taskList.appendChild(li);
    });
}
function addTask(e){
    e.preventDefault();
    if(taskInput.value === ''){
        alert("Please insert some task!");    
    }else {
        //Create li element
        const li = document.createElement('li');
        li.className = "collection-item";
        li.appendChild(document.createTextNode(taskInput.value));
        
        //Create a link 
        const link = document.createElement('a');
        link.className = 'delete-item secondary-content';
        link.setAttribute('href', '#');
        link.innerHTML = "<i class='fa fa-trash'></i>";
        
        li.appendChild(link);
        
        //Store to local storage
        storeTasksInLocalStorage(taskInput.value);
        
        taskList.appendChild(li);
        
        taskInput.value = "";
    }
}
function storeTasksInLocalStorage(task){
    let tasks;
    if(localStorage.getItem('tasks') === null){
        tasks = [];
    }else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    console.log(typeof tasks)
    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
}
function removeTask(e){
    if(e.target.classList.contains('delete-item') || e.target.parentElement.classList.contains('delete-item')){
        e.preventDefault();
        if(confirm("Are you sure you want to delete?")){
            let taskValue;
            
            if(e.target.parentElement.nodeName === "LI"){
                taskValue = e.target.parentElement.textContent;
                e.target.parentElement.remove();
            }else{
                taskValue = e.target.parentElement.parentElement.textContent;
                e.target.parentElement.parentElement.remove();
            }
            removeTaskFromLocalStorage(taskValue);
        }
    }
}
function removeTaskFromLocalStorage(taskValue){
    let tasks;
    if(localStorage.getItem('tasks') === null){
        tasks = [];
    }else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.forEach(function(task, index){
       if(task === taskValue){
           tasks.splice(index, 1);
       } 
    });
    localStorage.setItem('tasks', JSON.stringify(tasks));
}
function clearTasks(){
    let tasks = [];
    //Slower Method:
    //taskList.innerHTML = "";
    
    //FASTER METHOD:
    while(taskList.firstChild){
        taskList.removeChild(taskList.firstChild);
    }
    localStorage.setItem('tasks', JSON.stringify(tasks));
}
function filterTasks(e){
    const key = e.target.value.toLowerCase();
    
    document.querySelectorAll('.collection-item').forEach(function(task){
        const item = task.firstChild.textContent;
        if(item.toLowerCase().indexOf(key)===-1){
            task.style.display = 'none';
        }else{
            task.style.display = 'block';
        }
    });
}